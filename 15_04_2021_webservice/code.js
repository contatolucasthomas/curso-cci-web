function doGet(r) 
{
  var nome_completo = r.parameters.nome_completo.toString().trim()
  var senha = r.parameters.senha.toString().trim()
  var email = r.parameters.email.toString().trim()
  var partes = nome_completo.split(" ")
  var primeiro_nome = partes[0]
  partes.shift()
  var resto_nome = partes.join(" ")
  body = {
  "name": {
    "familyName": resto_nome,
    "givenName": primeiro_nome
  },
  "password": senha,
  "primaryEmail": email,
  "changePasswordAtNextLogin":"true"
  }
  try{
    var novo_usuario = AdminDirectory.Users.insert(body)
    return ContentService.createTextOutput(JSON.stringify({"Mensagem":"Usuário criado com sucesso! "+novo_usuario.id}))
  }catch(e){
  return ContentService.createTextOutput(JSON.stringify({
    "Status":"Falho",
    "Erro":e.toString()
  }))
  }
  return ContentService.createTextOutput(JSON.stringify(body))
}
